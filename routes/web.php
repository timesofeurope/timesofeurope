<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomepageController@home')->name('home');

Auth::routes();
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::get('/article-preview/{slug}/{token}', 'Manager\ArticlesController@preview')->name('manager.articles.preview');

Route::get('/author/{author}', 'HomepageController@author')->name('author');

Route::get('/s/{section}', 'HomepageController@section')->name('section');

Route::get('/a/{slug}/{name?}', 'ArticlesController@show')->name('articles.show');

Route::get('/invite/{token}', 'Manager\UserInvitesController@use')->name('manager.invite');
Route::post('/invite/redeem', 'Manager\UserInvitesController@redeem')->name('manager.invite.redeem');

Route::group(['prefix' => 'manager', 'middleware' => ['auth', 'permission:access manager']], function () {
    Route::get('/', 'Manager\ManagerController@home')->name('manager.home');
    Route::get('articles', 'Manager\ArticlesController@index')->name('manager.articles.index');
    Route::post('trash/{id}', 'Manager\ArticlesController@trash')->name('manager.articles.trash');
    Route::get('edit/', 'Manager\ArticlesController@edit');
    Route::get('edit/{id}', 'Manager\ArticlesController@edit')->name('manager.articles.edit');
    Route::post('edit/{id}', 'Manager\ArticlesController@save')->name('manager.articles.edit.save');
    Route::get('previewLoading', 'Manager\ArticlesController@previewLoading')->name('manager.articles.preview.loading');

    Route::get('users', 'Manager\UsersController@index')->name('manager.users.index');
    Route::get('users/{id}/edit', 'Manager\UsersController@edit')->name('manager.users.edit');
    Route::post('users/{id}/edit', 'Manager\UsersController@save')->name('manager.users.save');

    Route::get('roles', 'Manager\RolesController@index')->name('manager.roles.index');
    Route::get('roles/{id}/edit', 'Manager\RolesController@edit')->name('manager.roles.edit');
    Route::post('roles/{id}/edit', 'Manager\RolesController@save')->name('manager.roles.save');

    Route::get('users/invite-links', 'Manager\UserInvitesController@index')->name('manager.users.invites');
    Route::post('users/invite-links', 'Manager\UserInvitesController@create')->name('manager.users.invites.create');
    Route::post('users/invite-links/delete', 'Manager\UserInvitesController@delete')->name('manager.users.invites.delete');

    Route::get('sections', 'Manager\SectionsController@index')->name('manager.sections.index');
    Route::post('sections/reorder', 'Manager\SectionsController@reorder')->name('manager.sections.reorder');

    Route::get('frontpage', 'Manager\FrontpageController@edit')->name('manager.frontpage.edit');
});

Route::group(['prefix' => 'api/v1'], function () {
    Route::group(['prefix' => 'manager', 'middleware' => ['auth:web', 'permission:access manager']], function () {
        Route::post('image/upload', 'Manager\ImagesController@upload')->name('api.manager.image.upload');

        Route::post('section/new', 'Manager\SectionsController@save')->name('api.manager.section.new');
        Route::post('section/{id}', 'Manager\SectionsController@save')->name('api.manager.section.edit');

        Route::get('articles/many', 'Manager\ArticlesController@getMany')->name('api.manager.articles.getMany');
        Route::get('article/{id}', 'Manager\ArticlesController@get')->name('api.manager.article.get');
        Route::get('article/{id}/history', 'Manager\ArticlesController@history')->name('api.manager.article.history');

        Route::get('frontpage/getBlockFor/{id}', 'Manager\FrontpageController@getBlockFor')->name('api.manager.frontpage.getBlockFor');
        Route::get('frontpage/generateVariant', 'Manager\FrontpageController@generateVariant')->name('api.manager.frontpage.generateVariant');
        Route::post('frontpage/save', 'Manager\FrontpageController@save')->name('api.manager.frontpage.save');
    });
});

