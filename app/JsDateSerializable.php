<?php

namespace App;

use Carbon\Carbon;
use DateTimeInterface;

trait JsDateSerializable
{
    protected function serializeDate(DateTimeInterface $date)
    {
        return (new Carbon($date))->toW3cString();
    }
}