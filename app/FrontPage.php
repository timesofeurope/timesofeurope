<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FrontPage extends Model
{
    protected $fillable = ['data', 'user_id'];
    protected $casts = [
        'data' => 'array',
    ];
}
