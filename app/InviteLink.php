<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InviteLink extends Model
{
    protected $fillable = ['role_id', 'max_uses', 'expires_at', 'user_id', 'token'];
    protected $dates = ['created_at', 'updated_at', 'expires_at'];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function role() {
        return $this->belongsTo('Spatie\Permission\Models\Role');
    }
}
