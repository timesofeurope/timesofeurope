<?php

namespace App;

use App\Scopes\SectionOrderScope;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    protected $fillable = ['name', 'colour_hex', 'order', 'parent_id', 'description'];
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new SectionOrderScope);
    }

    use Sluggable;

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function articles() {
        return $this->hasMany('App\Article');
    }

    public function children() {
        return $this->hasMany('App\Section', 'parent_id', 'id');
    }
    public function parent() {
        return $this->belongsTo('App\Section', 'parent_id', 'id');
    }
}
