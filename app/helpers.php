<?php

/** Whether the user can perform any of the actions
 * @param $perms string|array Either an array of permissions, or permissions separated by a | (pipe)
 * @return bool Whether the user can perform any of the permissions
 */
function canAny($perms) {
    $user = Auth::user();
    if (!$user) {
        return false;
    }
    if (is_string($perms)) {
        $perms = explode('|', $perms);
    }
    foreach ($perms as $perm) {
        if ($user->can($perm)) {
            return true;
        }
    }
    return false;
}

/** Whether the user can perform all of the actions
 * @param $perms string|array Either an array of permissions, or permissions separated by a | (pipe)
 * @return bool Whether the user can perform all of the permissions
 */
function canAll($perms) {
    $user = Auth::user();
    if (!$user) {
        return false;
    }
    if (is_string($perms)) {
        $perms = explode('|', $perms);
    }
    foreach ($perms as $perm) {
        if (!$user->can($perm)) {
            return false;
        }
    }
    return true;
}

/** Generate a random string
 * @param int $length The length of the string
 * @return string The string
 */
function generateRandomString($length = 5)
{
    $characters = '0123456789bcdfghjklmnpqrstvwxyz';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

