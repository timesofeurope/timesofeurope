<?php

namespace App\Observers;

use App\Article;
use Illuminate\Support\Facades\Cache;

class ArticleObserver {
    public function saved (Article $article) {
        Cache::forget('article' . $article->id);
    }
}