<?php

namespace app\Http\ViewComposers;


use App\Section;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class SectionsComposer
{

    public function compose(View $view)
    {
        $view->with('sections', Section::whereNull('parent_id')->get());
        $view->with('allSections', Section::all());
    }
}