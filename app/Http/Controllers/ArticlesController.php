<?php

namespace App\Http\Controllers;

use App\Article;
use Illuminate\Http\Request;

class ArticlesController extends Controller
{
    public function show($slug) {
        $article = Article::where('slug', $slug)->first();

        if ($article === null) {
            abort(404);
        }

        return view('article', ['article' => $article]);
    }
}
