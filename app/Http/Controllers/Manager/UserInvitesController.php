<?php

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use App\InviteLink;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;

class UserInvitesController extends Controller
{
    public function index()
    {
        $expired = request()->input('expired') === 'on';
        $used = request()->input('used') === 'on';
        $query = InviteLink::on();
        if (!$expired) {
            $query = $query->where(function ($query) {
                $query->where('expires_at', '>=', DB::raw('NOW()'))
                    ->orWhereNull('expires_at');
            });
        }
        if (!$used) {
            $query = $query->where(function ($query) {
                $query->where('uses', '<', DB::raw('max_uses'))
                    ->orWhere('max_uses', 0);
            });
        }
        $links = $query->paginate(10);

        $roles = Role::where('can_be_invited', true)->get();

        return view('manager.users.invite-links', [
            'links' => $links,
            'roles' => $roles,
            'filters' => ['expired' => $expired, 'used' => $used]
        ]);
    }

    public function create()
    {
        $this->authorize('invite new users');
        $fields = [
            'role_id' => request()->input('role_id'),
            'max_uses' => request()->input('max_uses'),
            'user_id' => Auth::id(),
            'token' => generateRandomString(8)
        ];
        $expires = request()->input('expires');
        switch($expires) {
            case 'never':
                $fields['expires_at'] = null;
                break;
            case '1day':
                $fields['expires_at'] = Carbon::now()->addDay();
                break;
            case '1week':
                $fields['expires_at'] = Carbon::now()->addWeek();
                break;
        }

        // Quick sanity check
        if (!(Role::find($fields['role_id'])->can_be_invited)) {
            abort(403);
        }

        $link = InviteLink::create($fields);

        flash('Link created!')->success();

        return redirect()->back();
    }

    public function delete() {
        $this->authorize('invite new users');

        $link = InviteLink::find(request()->input('invite_link_id'));
        $link->delete();

        flash('Link deleted successfully.');
        return redirect()->back();
    }

    function use($token) {
        $link = InviteLink::where('token', $token)->first();

        if ($link === null) {
            abort(404);
        }
        if ($link->expires_at && $link->expires_at->lt(Carbon::now())) {
            return view('manager.users.invite.expired');
        }
        if ($link->uses >= $link->max_uses) {
            return view('manager.users.invite.used');
        }

        return view('manager.users.invite.use', ['link' => $link]);
    }

    function redeem() {
        $link = InviteLink::where('token', request()->input('invite_token'))->first();
        if ($link === null) {
            abort(404);
        }
        if ($link->expires_at && $link->expires_at->lt(Carbon::now())) {
            return view('manager.users.invite.expired');
        }
        if ($link->uses >= $link->max_uses) {
            return view('manager.users.invite.used');
        }

        $validated = request()->validate([
            'name' => 'required',
            'email' => 'required|email',
            'school' => 'required',
            'password' => 'required|min:8|confirmed',
            'password_confirmation' => 'required|min:8'
        ]);

        $validated['password'] = bcrypt($validated['password']);

        $user = User::create($validated);
        $user->assignRole($link->role);
        Auth::login($user);

        DB::table('invite_links')->whereId($link->id)->increment('uses');

        return view('manager.users.invite.redeemed');
    }
}
