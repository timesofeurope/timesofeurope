<?php

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use App\Section;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class SectionsController extends Controller
{
    public function index() {
        $sections = Section::all();

        return view('manager.sections.index', ['sections' => $sections]);
    }

    public function reorder() {
        $this->authorize("change front page");
        $data = request()->all()['sections'];
        foreach ($data as $id => &$datum) {
            if ($datum['parent_id'] === "null") {
                $datum["parent_id"] = null;
            }
            Section::find($id)->update($datum);
        }
        flash('Order updated.');
        return redirect()->back();
    }

    function save($id = null) {
        $this->authorize("change front page");
        $validated = request()->validate([
            'name' => '',
            'colour_hex' => '',
            'parent_id' => '',
            'description' => '',
            'order' => '',
        ]);
        if ($id === null) {
            $section = Section::create($validated);
        } else {
            $section = Section::find($id);
            $section->update($validated);
        }
        return response()->json([
            'error' => false
        ]);
    }
}
