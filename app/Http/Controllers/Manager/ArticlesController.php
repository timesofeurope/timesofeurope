<?php

namespace App\Http\Controllers\Manager;

use App\Article;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ArticlesController extends Controller
{
    function index()
    {
        $filterState = request()->input('filter.state');
        if ($filterState === 'all') {
            $filterState = null;
        }
        if ($filterState) {
            $articles = Article::where('state', $filterState)->whereNull('preview_parent_id')->get()->reverse();
        } else {
            $articles = Article::where('state', '!=', 'trashed')->whereNull('preview_parent_id')->get()->reverse();
        }
        return view('manager.articles.index', ['articles' => $articles, 'filterState' => $filterState]);
    }

    public function edit($id = null)
    {
        if ($id === null) {
            $article = new Article;
        } else {
            $article = Article::findOrFail($id);
        }

        $this->checkArticlePerms($article);

        $sections = DB::table('sections')->select(['id', 'name'])->get();

        $protected = null;
        if (Auth::user()->can('edit protected properties')) {
            $protected = [
                'users' => User::all()
            ];
        }

        if (request()->input('editor') === 'old') {
            $view = 'manager.articles.editor';
        } else {
            $view = 'manager.articles.editor-new';
        }

        return view($view, ['article' => $article, 'sections' => $sections, 'protected' => $protected]);
    }

    /** Check if the user has perms to change the article's author
     * If there is none given, there is nothing to check.
     * If the article already exists, check if it's the article's original author
     * If the article is new, check if it's the current user
     * Otherwise, check if the user has the "edit protected properties" perm
     * @param $userField int|null value of the user_id field
     * @param $article \App\Article The article to check
     * @return bool Whether the user has perms
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException if the user doesn't have perms
     */
    private function checkArticleUserFieldPerms($userField, $article) {
        if (empty($userField)) {
            return true;
        }
        if ($article->exists) {
            if ($userField === $article->user->id) {
                return true;
            }
        } else {
            if ($userField === Auth::id()) {
                return true;
            }
        }
        $this->authorize('edit protected properties');
        return true;
    }

    public function save()
    {
        try {
            $data = request()->all();
            $inputId = request()->input('article_id');

            // Create a new article if we're not editing one
            if ($inputId === 'null') {
                $article = new Article;
                $data['state'] = 'unedited';
            } else {
                $article = Article::findOrFail($data['article_id']);
            }

            // Check perms
            $this->checkArticlePerms($article);

            // Check if the user can change the user field
            $this->checkArticleUserFieldPerms($data['user_id'], $article);

            // Set if there is no ID provided
            if (empty($data['user_id'])) {
                $data['user_id'] = Auth::id();
            }

            // fill will hate us unless we unset these
            if (isset($data['preview_parent_id']) && $data['preview_parent_id'] === 'null') {
                unset($data['preview_parent_id']);
            }
            if (isset($data['article_id'])) {
                unset($data['article_id']);
            }
            if (isset($data['_token'])) {
                unset($data['_token']);
            }
            if (isset($data['files'])) {
                unset($data['files']);
            }

            // Find the preview holder, for later
            if (isset($data['preview_holder_id']) && $data['preview_holder_id'] !== 'null') {
                $holder = Article::find($data['preview_holder_id']);
                unset($data['preview_holder_id']);
            }

            // Mark as edited if the current user is an editor AND not the author
            if ($article->state === 'unedited' && Auth::id() !== $article->user->id && Auth::user()->hasRole('editor')) {
                $data['state'] = 'edited';
            }

            // Mark the article as published if the current user is the chief editor and hit the publish button
            if (Auth::user()->can('change front page') && $data['action'] === 'publish') {
                $data['state'] = 'published';
                flash('Article published.')->success();
            }

            // Ditto for yanking
            if (Auth::user()->can('change front page') && $data['action'] === 'yank') {
                $data['state'] = 'yanked';
                flash('Article yanked.')->warning();
            }

            if (preg_match_all('/src="(data:image\/[a-zA-Z0-9]+;base64,[a-zA-Z0-9+\/]+={0,2})/', $data['content'], $matches, PREG_SET_ORDER) !== FALSE) {
                $tempData = collect($data)->except('content')->all();
                $article->fill($tempData);
                $article->save();
                foreach ($matches as $match) {
                    $base64 = $match[0];
                    $file = $article->addMediaFromBase64($base64)->toMediaCollection('images');
                    $data['content'] = str_replace($base64, 'src="' . $file->getUrl(), $data['content']);
                }
            }

            if (request()->hasFile('cover_image')) {
                $tempData = collect($data)->except('content')->all();
                $article->fill($tempData);
                $article->save();

                $file = $article->addMediaFromRequest('cover_image')->toMediaCollection('cover-images');
                $article->cover_image_id = $file->id;
                $article->save();
            }
            unset($data['cover_image']);

            $shouldRegenerate = request()->input("crop_gravity") !== $article->crop_gravity;

            $article->fill($data);
            $article->save();

            // Regenerate thumbnails if we changed the crop gravity
            if ($shouldRegenerate) {
                Artisan::call("medialibrary:regenerate", ["--ids=" > $article->id]);
            }

            // Set the preview holder's parent ID
            if (isset($holder)) {
                $holder->preview_parent_id = $article->id;
                $holder->save();
            }

            flash('Article saved.');

            if (request()->isXmlHttpRequest()) {
                return response()->json(['error' => false, 'article' => $article]);
            } else {
                return redirect()->to(
                    route('manager.articles.edit', $article->id)
                );
            }
        } catch (HttpException $e) {
            if (request()->isXmlHttpRequest()) {
                return response()->json([
                    'error' => true,
                    'code' => $e->getStatusCode(),
                    'message' => $e->getMessage()
                ]);
            } else {
                throw $e;
            }
        }
    }

    public function preview($slug, $token)
    {
        $article = Article::where('slug', $slug)->first();
        if ($article->preview_token !== $token) {
            abort(404);
        }

        return view('article', ['article' => $article]);
    }

    public function previewLoading() {
        return view('manager.articles.waiting-for-preview');
    }

    public function trash($id)
    {
        $article = Article::find($id);
        if (empty($article)) {
            abort(404);
        }

        $this->checkArticlePerms($article);

        $article->state = 'trashed';
        $article->save();

        flash('Article trashed.');
        return redirect()->to(route('manager.articles.index'));
    }

    /**
     * @param $article
     */
    private function checkArticlePerms($article)
    {
        if ($article->user_id) {
            if ($article->user_id === Auth::id()) {
                $this->authorize('write articles');
            } else {
                $this->authorize('edit any article');
            }
        } else {
            $this->authorize('write articles');
        }
    }

    // ===========================================================
    // ============== API ROUTES START HERE ======================
    // ===========================================================

    public function get($id) {
        $article = Article::findOrFail($id);

        return response()->json([
            'error' => false,
            'article' => $article
        ]);
    }

    public function getMany() {
        $arra = request()->input('ids');
        $articles = Article::find($arra);

        return response()->json([
            'error' => false,
            'articles' => $articles
        ]);
    }

    public function history($id) {
        $article = Article::findOrFail($id);
        $history = $article->audits->map(function ($audit) {
            $user = User::find($audit->user_id);
            return [
                'revision' => $audit,
                'user' => $user->toArray(),
                'changes' => $audit->getModified()
            ];
        });

        return response()->json([
            'error' => false,
            'history' => $history
        ]);
    }

}
