<?php

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use App\InviteLink;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;

class UsersController extends Controller
{
    public function index()
    {
        $users = User::permission('access manager')->paginate(10);

        return view('manager.users.index', [
            'users' => $users
        ]);
    }

    public function edit($id) {
        if ($id !== Auth::id()) {
            $this->authorize('edit any user profile');
        }

        $user = User::findOrFail($id);

        $roles = Role::all();

        return view('manager.users.edit-profile', ['user' => $user, 'roles' => $roles]);
    }

    public function save(Request $request, $id) {
        if ($id !== Auth::id()) {
            $this->authorize('edit any user profile');
        }
        if ($request->input('roles') !== null) {
            $this->authorize('change any user roles');
        }

        $user = User::findOrFail($id);
        $validated = $request->validate([
            'name' => 'required',
            'school' => 'required',
            'email' => 'required|email',
            'facebook_url' => 'url',
            'roles.*' => '',
            'new_password' => 'min:8'
        ]);

        $user->update(collect($validated)->except(['roles', 'new_password'])->all());

        if ($request->input('new_password') !== null) {
            $user->password = bcrypt($request->input('new_password'));
            $user->save();
        }

        if ($request->input('roles') !== null) {
            $user->syncRoles(collect($request->input('roles'))->keys());
            flash('Roles updated.');
        }

        flash('User updated');

        return redirect()->back();
    }
}
