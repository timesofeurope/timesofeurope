<?php

namespace App\Http\Controllers\Manager;

use App\Article;
use App\FrontPage;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;

define("MAX_VARIANTS", 100);
define("ROW_MAX", 4);
define("NUM_ROWS_MIN", 3);
define("NUM_ROWS_MAX", 6);
define("NUM_ARTICLES_MIN", 10);
define("NUM_ARTICLES_MAX", 20);

class ImpossibleLayoutException extends \Exception
{
}

class FrontpageController extends Controller
{
    public function edit()
    {
        $this->authorize("change front page");

        $frontpage = FrontPage::orderBy('id', 'desc')->first();

        return view('manager.frontpage.edit', ['frontpage' => $frontpage]);
    }

    function attempt()
    {
        $articles = Article::where('state', 'published')
            ->orderBy('published_at', 'desc')
            ->limit(rand(NUM_ARTICLES_MIN, NUM_ARTICLES_MAX))
            ->get();

        foreach ($articles as $article) {
            Cache::put('article' . $article->id, $article);
        }

        $articles = collect([
            $articles->first(),
            $articles->slice(1)->shuffle()
        ])->flatten(1);
        $blacklist = [];
        $result = [];
        for ($i = 0, $max = rand(NUM_ROWS_MIN, NUM_ROWS_MAX); $i < $max; $i++) {
            $result[] = [];
        }
        for ($i = 0; $i < count($result); $i++) {
            $rowLen = 0;
            while ($rowLen < ROW_MAX) {
                $permissible = $articles->reject(function ($x) use ($blacklist) {
                    return in_array($x->id, $blacklist);
                });
                if (count($permissible) === 0) {
                    break;
                }
                $width = rand(1, ROW_MAX - $rowLen);
                $article = $permissible->first();
                $blacklist[] = $article->id;
                $result[$i][] = [$article->id, $width];
                $rowLen += $width;
            }
        }
        $lastWidth = collect($result[count($result) - 1])->reduce(function ($carry, $x) {
            return $carry + $x[1];
        }, 0);
        if ($lastWidth !== ROW_MAX) {
            throw new ImpossibleLayoutException();
        }
        return $result;
    }

    function score($layout)
    {
        $RULES = [
            // Reward wide articles with cover images
            function (Collection $collection) {
                return $collection->flatten(1)->filter(function ($pair) {
                        $article = Article::get($pair[0]);
                        return $article->cover_image_id !== null && $pair[1] > 2;
                    })->count() * 20;
            },
            // SUPER WIDE IMAGES ARE GULAG
            function (Collection $collection) {
                return $collection->flatten(1)->filter(function ($pair) {
                        $article = Article::get($pair[0]);
                        return $article->cover_image_id !== null && $pair[1] === 4;
                    })->count() * 50;
            },
            // Conversely, penalize narrow images, they're fugly
            function (Collection $collection) {
                return $collection->flatten(1)->filter(function ($pair) {
                        $article = Article::get($pair[0]);
                        return $article->cover_image_id !== null && $pair[1] === 1;
                    })->count() * -15;
            },
            // And penalize wide no-image articles
            function (Collection $collection) {
                return $collection->flatten(1)->filter(function ($pair) {
                        $article = Article::get($pair[0]);
                        return $article->cover_image_id === null && $pair[1] > 2;
                    })->count() * -50;
            },
            // Penalize identical adjacent rows
            function (Collection $collection) {
                $result = 0;
                $previous = [];
                foreach ($collection as $row) {
                    $widths = collect($row)->map(function($x) { return $x[1]; })->toArray();
                    if ($widths == $previous) {
                        $result -= 91;
                    }
                    $previous = $widths;
                }
            },
            // Asymmetry
            function (Collection $collection) {
                $result = 0;
                $previous = [];
                foreach ($collection as $row) {
                    if ($row[0][1] === 2) {
                        continue;
                    }
                    $widths = collect($row)->map(function($x) { return $x[1]; })->toArray();
                    if ($widths == array_reverse($previous)) {
                        $result += 30;
                    }
                    $previous = $widths;
                }
            }
        ];

        $collection = collect($layout);
        $score = 0;
        foreach ($RULES as $rule) {
            $score += $rule($collection);
        }
        return $score;
    }

    function generateVariant()
    {
        $seed = request()->input("seed", rand());
        srand($seed);

        $variants = collect();
        $attempts = 0;
        makeLayouts:
        for ($i = 0; $i < MAX_VARIANTS; $i++) {
            $attempts++;
            try {
                $variants->push($this->attempt());
            } catch (ImpossibleLayoutException $ignored) {}
        }

        if ($variants->count() === 0) {
            goto makeLayouts;
        }

        $scores = $variants->map(function ($variant) {
            return array_merge([
                'data' => $variant,
                'score' => $this->score($variant),
            ]);
        });
        $scores->sortBy('score')->reverse();
        $winner = $scores->first();
        $lowest = $scores->sortBy('score')->first()['score'];

        return response()->json([
            'error' => false,
            'data' => $winner,
            'meta' => [
                'lowestScore' => $lowest,
                'attempts' => $attempts,
                'successfulAttempts' => $variants->count(),
                'MAX_VARIANTS' => MAX_VARIANTS,
                'seed' => $seed
            ]
        ]);
    }

    public function getBlockFor($id)
    {
        $article = Article::findOrFail($id);
        $width = request()->input('width') ?? 1;
        return view('components.frontpage-block', [
            'width' => $width,
            'article' => $article,
            'link' => request()->input('link', 'true') === 'true'
        ]);
    }

    public function save() {
        $this->authorize("change front page");

        $variant = request()->input('data');
        $frontpage = FrontPage::create([
            'data' => $variant,
            'user_id' => Auth::id()
        ]);

        return response()->json([
            'error' => false,
            'id' => $frontpage->id
        ]);
    }
}
