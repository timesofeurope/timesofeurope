<?php

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolesController extends Controller
{
    public function index()
    {
        $roles = Role::paginate(10);

        return view('manager.roles.index', [
            'roles' => $roles
        ]);
    }

    public function edit($id) {
        $this->authorize('change any user roles');

        $role = Role::findOrFail($id);

        $permissions = Permission::all();

        return view('manager.roles.edit', ['role' => $role, 'permissions' => $permissions]);
    }

    public function save(Request $request, $id) {
        $this->authorize('change any user roles');

        $role = Role::findOrFail($id);
        $validated = $request->validate([
            'name' => 'required',
            'permissions.*' => '',
        ]);

        $role->update(collect($validated)->except(['permissions'])->all());

        if ($request->input('permissions') !== null) {
            $role->syncPermissions(collect($request->input('roles'))->keys());
            flash('Permissions updated.');
        }

        flash('Role updated');

        return redirect()->back();
    }
}
