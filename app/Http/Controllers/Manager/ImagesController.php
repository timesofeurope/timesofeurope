<?php

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ImagesController extends Controller
{
    public function upload()
    {
        $path = request()->file('image')->store('temp');

        return response()->json([
            'error' => false,
            'filePath' => $path,
            'url' => Storage::url($path)
        ]);
    }
}
