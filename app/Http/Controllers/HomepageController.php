<?php

namespace App\Http\Controllers;

use App\Article;
use App\FrontPage;
use App\Section;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomepageController extends Controller
{
    function home()
    {
        $frontpage = FrontPage::orderBy('id', 'desc')->first();

        $articleIds = collect($frontpage->data)->flatMap(function($x) {
            return collect($x)->map(function ($x) {
                return $x[0];
            })->toArray();
        })->toArray();

        $articles = Article::find($articleIds)->keyBy(function($x) { return $x->id; });


        return view('new-frontpage', ['articles' => $articles, 'data' => $frontpage->data]);
    }

    function section($sectionSlug)
    {
        $section = Section::where('slug', $sectionSlug)->first();
        $childSections = $section->children;
        $articles = Article::where(function ($query) use ($section, $childSections) {
            $query->where('section_id', $section->id);
            $query->orWhereIn('section_id', $childSections->map(function($s) { return $s->id; }));
        })->where('state', 'published')->orderBy('published_at', 'desc')->get();

        return view('section', ['section' => $section, 'articles' => $articles]);
    }

    function author($authorSlug)
    {
        $author = User::where('slug', $authorSlug)->first();
        $articles = $author->articles()->where('state', 'published')->orderBy('published_at', 'desc')->get();

        return view('author', ['author' => $author, 'articles' => $articles]);
    }
}
