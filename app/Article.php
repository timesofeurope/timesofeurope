<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Auditable;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;
use Spatie\MediaLibrary\Media;

/**
 * @property string $link
 * @property string $state
 * @property Article $preview_holders
 * @property Article $preview_parent
 * @property User $user
 * @property Section $section
 */
class Article extends Model implements HasMediaConversions, AuditableContract
{
    use HasMediaTrait, JsDateSerializable, Auditable;
    protected $fillable = ['id', 'title', 'section_id', 'user_id', 'content', 'state', 'published_at', 'preview_parent_id', 'cover_image_id', 'crop_gravity'];
    protected $dates = ['created_at', 'updated_at', 'published_at'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function section()
    {
        return $this->belongsTo('App\Section');
    }

    public function previewHolder()
    {
        return $this->hasOne('App\Article', 'preview_parent_id');
    }

    public function previewParent()
    {
        return $this->belongsTo('App\Article', 'preview_parent_id');
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($article) {
            $article->attributes['slug'] = Article::generateSlug();
            $article->attributes['preview_token'] = generateRandomString(64);
        });
    }

    public static function get($id) {
        return Cache::remember('article' . $id, 60, function () use ($id) {
            return Article::findOrFail($id);
        });
    }


    /** @noinspection PhpInconsistentReturnPointsInspection */
    protected static function generateSlug()
    {
        while (true) {
            $slug = generateRandomString();
            if (!(Article::where('slug', $slug)->exists())) {
                return $slug;
            }
        }
    }

    public function getLinkAttribute()
    {
        return route('articles.show',
            ['slug' => $this->attributes['slug'],
                'name' => str_slug($this->attributes['title'], '-')]
        );
    }

    public $registerMediaConversionsUsingModelInstance = true;

    public function registerMediaConversions(Media $media = null)
    {
        $gravity = "";
        switch ($this->crop_gravity) {
            case 'middle':
                $gravity = Manipulations::CROP_CENTER;
                break;
            case 'top':
                $gravity = Manipulations::CROP_TOP;
                break;
            case 'bottom':
                $gravity = Manipulations::CROP_BOTTOM;
                break;
        }
        $this->addMediaConversion('thumb')
            ->width(368)
            ->height(232)
            ->sharpen(10);
        $this->addMediaConversion('preview')
            ->width(12)
            ->height(12);
        $this->addMediaConversion('fb-cover')
            ->crop($gravity, 1200, 630)
            ->performOnCollections('cover-images');
        $this->addMediaConversion('frontpage')
            ->brightness(-10)
            ->width(800)
            ->performOnCollections("cover-images");
        $this->addMediaConversion('frontpage-preview')
            ->brightness(-10)
            ->width(12)
            ->height(12)
            ->performOnCollections("cover-images");
    }
}
