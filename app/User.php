<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use OwenIt\Auditing\Contracts\UserResolver;
use Spatie\Permission\Traits\HasRoles;

/**
 * @property mixed|string $avatar
 * @property mixed $articles
 */
class User extends Authenticatable implements UserResolver
{
    use Notifiable, Sluggable, HasRoles, ExposePermissions;

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'school', 'facebook_url'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function articles()
    {
        return $this->hasMany('App\Article');
    }

    public function inviteLinks() {
        return $this->hasMany('App\InviteLink');
    }

    public function getAvatarAttribute() {
        if (!empty($this->avatar_url)) {
            return $this->avatar_url;
        }
        return 'https://www.gravatar.com/avatar/' . md5(strtolower(trim($this->email))) . '?d=mm';
    }

    /**
     * Resolve the ID of the logged User.
     *
     * @return mixed|null
     */
    public static function resolveId()
    {
        return Auth::check() ? Auth::user()->getAuthIdentifier() : null;
    }
}
