@extends('layouts.app')

@section('body-class', 'homepage')

@section('content')
  @include('_public-nav-desktop')
  @include('_public-nav-mobile')
  <div class="home-articles">
    <div class="avatar-container">
      <img src="{{ $author->avatar }}" alt="Avatar of {{ $author->name }}" class="img-circle" width="80" height="80">
      <div>
        <h1>{{ $author->name }}</h1>
        <em>{{ $author->school }}</em>
      </div>
    </div>
    @foreach ($articles as $article)
      @component('home-article', ['article' => $article])
      @endcomponent
    @endforeach
  </div>
@endsection