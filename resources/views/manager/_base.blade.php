@extends('layouts.app')
@section('title', config('app.name') . " Manager")
@section('body-class', 'manager')

@push('header-styles')
  <link rel="stylesheet" href="{{ mix('css/manager.css') }}">
@endpush

@push('body-scripts-before-app')
  @routes
@endpush

@section('content')
  <div class="manager-container">
    @yield('replace-nav', View::make('manager._nav'))
    <main>
      @yield('flash', View::make('flash::message'))
      @yield('main')
    </main>
  </div>
@endsection