@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        <h1>Welcome to {{ config('app.name') }}!</h1>
        <p>You have been invited by {{ $link->user->name }} to join the {{ config('app.name') }} team.
        <p>Please quickly fill out this form and we'll get your Manager account set up.</p>

        @if ($errors->any())
          <div class="alert alert-danger">
            <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
        @endif

        <form action="{{ route('manager.invite.redeem') }}" method="post">
          <input type="hidden" name="invite_token" value="{{ $link->token }}">
          <div class="form-group">
            <label>
              Your Name
              <input type="text" name="name" class="form-control">
            </label>
          </div>
          <div class="form-group">
            <label>
              Your Email Address
              <input type="email" name="email" class="form-control">
            </label>
          </div>
          <div class="form-group">
            <label>
              Which school are you from?
              <input type="text" name="school" class="form-control">
            </label>
          </div>
          <div class="form-group">
            <label>
              Set a password
              <span id="passwordMountPoint"></span>
            </label>
          </div>
          <div class="form-group">
            <label>
              Type it again, just to be sure you typed it correctly
              <input type="password" name="password_confirmation" class="form-control">
            </label>
          </div>
          {{ csrf_field() }}
          <input type="submit" class="btn btn-success">
        </form>
      </div>
    </div>
  </div>
@endsection

@push('body-scripts')
  <script>
    window.mountPasswordField(document.getElementById("passwordMountPoint"), "password");
  </script>
@endpush