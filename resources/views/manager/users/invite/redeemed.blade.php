@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        <h1>Account created!</h1>
        <p>Your account has been created successfully!</p>
        <a href="{{ route('manager.home') }}" class="btn btn-primary">Go To Manager</a>
      </div>
    </div>
  </div>
@endsection