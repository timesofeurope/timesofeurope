@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        <h1>Invite used too many times</h1>
        <p>Your invite link has been used too many times. Please ask whoever gave it to you for a new one.</p>
      </div>
    </div>
  </div>
@endsection