@extends('layouts.app')

@section('content')
  <h1>Invite expired</h1>
  <p>Your invite link has expired. Please ask whoever gave it to you for a new one.</p>
@endsection