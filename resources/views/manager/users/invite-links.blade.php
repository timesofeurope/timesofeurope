@extends('manager._base')

@section('main')
  <h1>Invite Links</h1>
  <form action="{{ route('manager.users.invites.create') }}" method="post">
    <h2>Create New</h2>
    <div class="form-group">
      <label>
        New User's Role
        <select name="role_id" class="form-control">
          @foreach ($roles as $role)
            <option value="{{ $role->id }}">{{ title_case($role->name) }}</option>
          @endforeach
        </select>
      </label>
    </div>
    <div>
      <small>
        <b>Why are some roles not visible?</b> Some privileged roles cannot have users added directly via invite link.
        Invite the users to any other role and manually give them the role you want.
      </small>
    </div>
    <div class="form-group">
      <label>
        Max Number of Uses (0 for unlimited)
        <input type="number" name="max_uses" min="0" value="0">
      </label>
    </div>
    <div class="form-group">
      <label>
        Expiration
        <div class="btn-group" data-toggle="buttons">
          <label class="btn btn-primary active">
            Never
            <input type="radio" id="expiration" name="expires" value="never">
          </label>
          <label class="btn btn-primary">
            In One Day
            <input type="radio" id="expiration" name="expires" value="1day">
          </label>
          <label class="btn btn-primary">
            In One Week
            <input type="radio" id="expiration" name="expires" value="1week">
          </label>
        </div>
      </label>
    </div>
    {{ csrf_field() }}
    <input type="submit" class="btn btn-primary">
  </form>

  <hr>

  <form id="articlesFilterForm" method="get" action="{{ route('manager.users.invites') }}">
    <label>
      Show Expired
      <input type="checkbox" name="expired" {{ $filters['expired'] ? 'checked' : '' }} onclick="onFilterSelectChange()">
    </label>
    <label>
      Show Used
      <input type="checkbox" name="used" {{ $filters['used'] ? 'checked' : '' }} onclick="onFilterSelectChange()">
    </label>
  </form>

  <table class="table">
    <thead>
    <tr>
      <td>New User Role</td>
      <td>Uses</td>
      <td>Max Uses</td>
      <td>Expires</td>
      <td>Created By</td>
      <td></td>
    </tr>
    </thead>
    <tbody>
    @foreach($links as $link)
      <tr>
        <td>
          <a target="_blank" href="{{ route('manager.invite', $link->token) }}">{{ title_case($link->role->name) }}</a>
        </td>
        <td>
          {{ $link->uses }}
        </td>
        <td>
          {{ $link->max_uses }}
        </td>
        <td>
          @if ($link->expires_at)
            <span title="{{ $link->expires_at->toDayDateTimeString() }}">
              {{ $link->expires_at->diffForHumans() }}
            </span>
          @else
            Never
          @endif
        </td>
        <td>
          {{ $link->user->name }}
        </td>
        <td>
          <form action="{{ route('manager.users.invites.delete') }}" method="post">
            <input type="hidden" name="invite_link_id" value="{{ $link->id }}">
            {{ csrf_field() }}
            <button class="btn btn-danger">delete</button>
          </form>
        </td>
      </tr>
    @endforeach
    </tbody>
  </table>

  {{ $links->links() }}
@endsection

@push('body-scripts')
  <script>
      function onFilterSelectChange() {
          document.getElementById('articlesFilterForm').submit();
      }
  </script>
@endpush