@extends('manager._base')

@section('main')
  <h1>Users</h1>

  <table class="table">
    <thead>
    <tr>
      <td>
        Name
      </td>
      <td>
        School
      </td>
      <td>
        Email
      </td>
      <td>
        Roles
      </td>
      <td>Articles</td>
    </tr>
    </thead>
    <tbody>
    @foreach($users as $user)
      <tr>
        <td>
          <a href="{{ route('manager.users.edit', $user->id) }}">
            {{ $user->name }}
          </a>
        </td>
        <td>
          {{ $user->school }}
        </td>
        <td>
          {{ $user->email }}
        </td>
        <td>
          {{ $user->roles->map(function($x) { return title_case($x->name); })->implode('name', ', ') }}
        </td>
        <td>
          {{ $user->articles->where('state', 'published')->count() }}
        </td>
      </tr>
    @endforeach
    </tbody>
  </table>

  {{ $users->links() }}
@endsection
