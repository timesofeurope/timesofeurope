@extends('manager._base')

@section('main')
  <h1>Editing {{ $user->name }}</h1>

  @if ($errors->any())
    <div class="alert alert-danger">
      <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif

  <form method="post" action="{{ route('manager.users.save', $user->id) }}">
    <div class="form-group">
      <label>
        Name
        <input class="form-control" name="name" type="text" value="{{ $user->name }}" required>
      </label>
    </div>

    <div class="form-group">
      <label>
        School
        <input class="form-control" name="school" type="text" value="{{ $user->school }}" required>
      </label>
    </div>

    <div class="form-group">
      <label>
        Email
        <input class="form-control" name="email" type="email" value="{{ $user->email }}" required>
      </label>
    </div>

    <div class="form-group">
      <label>
        Facebook Profile URL
        <input class="form-control" name="facebook_url" type="url" value="{{ $user->facebook_url }}">
      </label>
      <small class="help-block">This will be used to associate articles with their author if they are shared on Facebook. This can be left blank if you wish.</small>
    </div>

    <div class="form-group">
      <label id="changePasswordParent">
        Change Password
        <button class="btn btn-default" onclick="window.changePassword(event)" id="changePassword">Change Password</button>
      </label>
    </div>

    @can('change any user roles')
      <div class="well">
        <h2>Roles</h2>
        @foreach ($roles as $role)
          <label>
            <input type="checkbox" name="roles[{{ $role->name }}]" {{ $user->hasRole($role) ? 'checked' : '' }}>
            {{ title_case($role->name) }}
          </label>
        @endforeach
      </div>
    @endcan

    {{ csrf_field() }}

    <input type="submit" class="btn btn-primary">
  </form>
@endsection

@push('body-scripts')
  <script>
    function changePassword(event) {
        event.preventDefault();

        var div = document.createElement('div');
        var parent = document.getElementById('changePasswordParent');
        var child = document.getElementById('changePassword');

        parent.replaceChild(div, child);
        window.mountPasswordField(div, 'new_password');

        return false;
    }
  </script>
@endpush
