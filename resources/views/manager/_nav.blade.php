@php
  $sidenavPages = [
    [
      'name' => 'Manage Articles',
      'link' => route('manager.articles.index'),
      'children' => [
        [
          'name' => 'Write New Article',
          'link' => route('manager.articles.edit', null),
          'highlight' => 'never',
          'permissions' => 'write articles'
        ],
        [
          'name' => 'Edit',
          'link' => route('manager.articles.index'),
          'highlight' => request()->url() === route('manager.articles.edit', null),
          'permissions' => 'write articles|edit any article'
        ]
      ]
    ], [
      'name' => 'User Management',
      'permissions' => 'edit any user profile|change any user roles|invite new users',
      'link' => '#',
      'children' => [
        [
          'name' => 'Users',
          'permissions' => 'edit any user profile|change any user roles',
          'link' => route('manager.users.index')
        ],
        [
          'name' => 'Roles',
          'permissions' => 'change any user roles',
          'link' => route('manager.roles.index')
        ],
        [
          'name' => 'Invite New Users',
          'permissions' => 'invite new users',
          'link' => route('manager.users.invites')
        ]
      ]
    ],
    [
      'name' => 'Sections',
      'permissions' => 'change front page',
      'link' => route('manager.sections.index')
    ],
    [
      'name' => 'Front Page',
      'permissions' => 'change front page',
      'link' => route('manager.frontpage.edit')
    ],
    [
      'name' => 'Signed in as ' . Auth::user()->name,
      'children' => [
        [
          'name' => 'Update My Profile',
          'link' => route('manager.users.edit', Auth::id())
        ]
      ]
    ],
    [
      'name' => 'Sign Out',
      'link' => route('logout')
    ]
  ];
@endphp

@include('manager._nav-desktop')
@include('manager._nav-mobile')