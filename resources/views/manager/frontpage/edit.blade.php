@extends('manager._base')

@section('main')
  <h1>Front Page</h1>

  <div id="frontpageMountPoint">
    <b>Front page editor loading, please wait...</b>
  </div>
  {{ csrf_field() }}

@endsection

@push('body-scripts')
  <script>
    window.mountFrontpage(document.getElementById('frontpageMountPoint'), {frontpage: {!! $frontpage->toJson() !!}});
  </script>
@endpush
