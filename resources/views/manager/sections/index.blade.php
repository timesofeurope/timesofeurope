@extends('manager._base')

@section('main')
  <h1>Sections</h1>

  <div id="sectionsMountPoint">
    <b>Section editor loading, please wait...</b>
  </div>
  {{ csrf_field() }}

@endsection

@push('body-scripts')
  <script>
      window.mountSections(document.getElementById('sectionsMountPoint'), {!! $sections->toJson() !!});
  </script>
@endpush
