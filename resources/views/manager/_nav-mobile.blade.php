<nav class="navbar navbar-inverse hidden-md hidden-lg">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse"
              aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">{{ config('app.name') }}</a>
    </div>

    <div class="collapse navbar-collapse" id="navbar-collapse">
      <ul class="nav navbar-nav">
        @foreach($sidenavPages as $section)
          @php
            $active = false;
            if (array_has($section, 'link')) {
              $active = request()->url() === $section['link'] && empty($section['children']);
            }
            if (!$active && !empty($section['children'])) {
              foreach ($section['children'] as $child) {
                if (request()->url() === $child['link']) {
                  $active = true;
                  break;
                }
              }
            }
            $can = true;
            if (array_has($section, 'permissions')) {
              $can = canAny($section['permissions']);
            }
          @endphp
          @if ($can)
            @if (empty($section['children']))
              <li class="{{ $active ? 'active' : '' }}">
                @if ($active)
                  <span class="sr-only">(current)</span>
                @endif
                @if(array_has($section, 'link'))
                  <a href="{{ $section['link'] }}">{{ $section['name'] }}</a>
                @else
                  <a href="#">{{ $section['name'] }}</a>
                @endif

              </li>
            @else
              <li class="dropdown {{ $active ? 'active' : '' }}">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                   aria-expanded="false">{{ $section['name'] }} <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  @if (array_has($section, 'link'))
                    <li><a href="{{ $section['link'] }}">{{ $section['name'] }}</a></li>
                  @endif
                  @foreach($section['children'] as $child)
                    @php
                      if (array_has($child, 'link')) {
                        $childActive = request()->url() === $child['link'];
                      }
                      if (array_has($child, 'highlight')) {
                        switch ($child['highlight']) {
                          case 'always':
                            $childActive = true;
                            break;
                          case 'never':
                            $childActive = false;
                            break;
                          default:
                            $childActive = $child['highlight'];
                        }
                      }
                      $canChild = true;
                      if (array_has($child, 'permissions')) {
                        $canChild = canAny($child['permissions']);
                      }
                    @endphp
                    @if ($canChild)
                      <li class="{{ $childActive ? 'active' : '' }}">
                        <a href="{{ $child['link'] }}">
                          {{ $child['name'] }}
                        </a>
                      </li>
                    @endif
                  @endforeach
                </ul>
              </li>
            @endif
          @endif
        @endforeach
      </ul>
    </div>
  </div>
</nav>