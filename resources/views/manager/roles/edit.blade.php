@extends('manager._base')

@section('main')
  <h1>Editing {{ $role->name }}</h1>

  @if ($errors->any())
    <div class="alert alert-danger">
      <ul>
        @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif

  <form method="post" action="{{ route('manager.roles.save', $role->id) }}">
    <div class="form-group">
      <label>
        Name
        <input class="form-control" name="name" type="text" value="{{ $role->name }}" required>
      </label>
    </div>


    <div class="well">
      <h2>Permissions</h2>
      @foreach ($permissions as $permission)
        <label>
          <input type="checkbox"
                 name="roles[{{ $permission->name }}]"
              {{ $role->hasPermissionTo($permission) ? 'checked' : '' }}
          >
          {{ title_case($permission->name) }}
        </label>
      @endforeach
    </div>

    {{ csrf_field() }}

    <input type="submit" class="btn btn-primary">
  </form>
@endsection

