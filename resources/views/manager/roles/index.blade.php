@extends('manager._base')

@section('main')
  <h1>Roles</h1>

  <table class="table">
    <thead>
    <tr>
      <td>
        Name
      </td>
      <td>
        Users
      </td>
      <td>
        Permissions
      </td>
    </tr>
    </thead>
    <tbody>
    @foreach($roles as $role)
      <tr>
        <td>
          <a href="{{ route('manager.roles.edit', $role->id) }}">
            {{ $role->name }}
          </a>
        </td>
        <td>
          {{ $role->users->count() }}
        </td>
        <td>
          {{ $role->permissions->map(function($x) { return title_case($x->name); })->implode(', ') }}
        </td>
      </tr>
    @endforeach
    </tbody>
  </table>

  {{ $roles->links() }}
@endsection
