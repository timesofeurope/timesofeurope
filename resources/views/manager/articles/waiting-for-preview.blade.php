@extends('manager._base')

@section('replace-nav', '')

@section('main')
  <h1>Generating article preview...</h1>

  <p>We're creating a preview of your edits for you. It will automatically appear here in a second.</p>
@endsection
