@extends('manager._base')

@php
  define('STATE_CLASSES', [
    'unedited' => 'info',
    'edited' => 'warning',
    'published' => 'success',
    'yanked' => 'danger',
    'trashed' => 'muted',
  ]);
@endphp

@section('main')
  <h1>
    @if ($filterState)
      {{ title_case($filterState) }} Articles
    @else
      Articles
    @endif
  </h1>

  <form id="articlesFilterForm" method="get" action="{{ route('manager.articles.index') }}">
    <select name="filter[state]" onchange="this.form.submit()">
      <option value="all" {{ empty($filterState) ? 'selected' : '' }}>All</option>
      @foreach (array_keys(STATE_CLASSES) as $key)
        <option value="{{ $key }}" {{ $filterState === $key ? 'selected' : '' }}>{{ title_case($key) }}</option>
      @endforeach
    </select>
  </form>

  <a href="{{ route('manager.articles.edit', null) }}" class="btn btn-primary">Write New</a>

  <table class="list">
    @foreach ($articles as $article)
      @php
        if (Auth::user()->can('edit any article')) {
          $canEdit = true;
        } else {
          $canEdit = $article->user->id === Auth::id();
        }
      @endphp
      <tr class="list-item">
        <td class="list-item-title">
          @if ($canEdit)
            <a href="{{ route('manager.articles.edit', $article) }}">{{ $article->title }}</a>
          @else
            @if ($article->state === 'published')
              <a href="{{ route('$article', $article->slug) }}">{{ $article->title }}</a>
            @else
              <span>{{ $article->title }}</span>
            @endif
          @endif
        </td>
        <td><span class="list-item-section">{{ $article->section->name }}</span></td>
        <td><span class="list-item-author">{{ $article->user->name }}</span></td>
        <td><span class="label label-{{ STATE_CLASSES[$article->state] }}">{{ title_case($article->state) }}</span></td>
        <td class="buttons">
          @if ($canEdit)
            <a href="{{ route('manager.articles.edit', $article) }}" class="btn btn-primary">EDIT</a>
          @else
            <a href="#" disabled class="btn disabled">EDIT</a>
          @endif
          <a
              href="{{ route('manager.articles.preview', ['slug' => $article->slug, 'token' => $article->preview_token ]) }}"
              class="btn"
              target="_blank"
          >
            PREVIEW
          </a>
          @if ($canEdit)
            <form method="post" action="{{ route('manager.articles.trash', $article->id) }}">
              {{ csrf_field() }}
              <button class="btn btn-danger">TRASH</button>
            </form>
          @else
            <a class="btn disabled" disabled href="#">TRASH</a>
          @endif
        </td>
      </tr>
    @endforeach
  </table>
@endsection

@push('body-scripts')
  <script>
      function onFilterSelectChange() {
          document.getElementById('articlesFilterForm').submit();
      }
  </script>
@endpush