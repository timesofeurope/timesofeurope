@extends('manager._base')

@section('main')
  <div id="editor-mount-point">Editor loading, please wait...</div>
@endsection

@push('body-scripts')
  <script>
    window.mountEditor(document.getElementById('editor-mount-point'), {
        article: {!! $article->exists ? $article->toJson() : json_encode([
            'id' => null
          ]) !!},
        previewHolder: {!! $article->previewHolder === null ? 'null' : $article->previewHolder->toJson() !!},
        user: {!! Auth::user()->toJson() !!},
        sections: {!! $sections->toJson() !!},
        userPermissions: {!! json_encode(Auth::user()->can) !!},
        protected: {!! json_encode($protected) !!}
    });
  </script>
@endpush