<nav class="sidebar hidden-xs hidden-sm">
  <div class="side-heading">
    <a href="/">{{ config('app.name') }}</a>
  </div>
  @foreach($sidenavPages as $section)
    @php
      $active = false;
      if (array_has($section, 'link')) {
        $active = request()->url() === $section['link'] && empty($section['children']);
      }
      if (!$active && !empty($section['children'])) {
        foreach ($section['children'] as $child) {
          if (request()->url() === $child['link']) {
            $active = true;
            break;
          }
        }
      }
      $can = true;
      if (array_has($section, 'permissions')) {
        $can = canAny($section['permissions']);
      }
    @endphp

    @if ($can)
      <div class="side-section {{ $active ? 'active' : '' }}">

        @if(array_has($section, 'link'))
          <a href="{{ $section['link'] }}" class="side-section-name">{{ $section['name'] }}</a>
        @else
          <span class="side-section-name">{{ $section['name'] }}</span>
        @endif

        @if(!empty($section['children']))
          <div class="side-section-children">
            @foreach($section['children'] as $child)
              @php
                if (array_has($child, 'link')) {
                  $childActive = request()->url() === $child['link'];
                }
                if (array_has($child, 'highlight')) {
                  switch ($child['highlight']) {
                    case 'always':
                      $childActive = true;
                      break;
                    case 'never':
                      $childActive = false;
                      break;
                    default:
                      $childActive = $child['highlight'];
                  }
                }

              $canChild = true;
              if (array_has($child, 'permissions')) {
                $canChild = canAny($child['permissions']);
              }
              @endphp

              @if ($canChild)
                <div class="side-section-child {{ $childActive ? 'active' : '' }}">

                  <a href="{{ $child['link'] }}">
                    {{ $child['name'] }}
                  </a>

                </div>
              @endif

            @endforeach
          </div>
        @endif

      </div>
    @endif
  @endforeach
</nav>