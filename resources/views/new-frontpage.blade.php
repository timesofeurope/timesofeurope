@extends('layouts.app')

@section('body-class', 'homepage')

@section('content')
  @include('_public-nav-desktop')
  @include('_public-nav-mobile')
  <div class="frontpage">
    @foreach ($data as $row)
      <div class="frontpage-row">
        @foreach ($row as $pair)
          @component('components.frontpage-block', ['article' => $articles[$pair[0]], 'width' => $pair[1]])
          @endcomponent
        @endforeach
      </div>
    @endforeach
  </div>
@endsection