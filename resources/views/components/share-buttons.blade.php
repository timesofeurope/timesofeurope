<div class="share-buttons">
  Share this article:
  <a href="#" onclick="window.fbShare('{{ Request::url() }}')" class="btn share-button share-button-facebook">
    Facebook
  </a>
  <a target="_blank" href="https://api.whatsapp.com/send?text={{ urlencode(Request::url()) }}" class="btn share-button share-button-whatsapp">
    WhatsApp
  </a>
</div>

@push('body-scripts')
  <script>
      window.fbAsyncInit = function() {
          FB.init({
              appId            : '{{ env('FACEBOOK_ID') }}',
              xfbml            : false,
              version          : 'v2.10'
          });
      };

      window.fbShare = function(url) {
          FB.ui({
              method: 'share',
              href: url
          });
      };

      (function(d, s, id){
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) {return;}
          js = d.createElement(s); js.id = id;
          js.src = "https://connect.facebook.net/en_US/sdk.js";
          fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));
  </script>
@endpush
