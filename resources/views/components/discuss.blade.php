<b>Discuss this article</b>

<div id="disqus_thread"></div>

@push('body-scripts')
  <script>
      var disqus_config = function () {
          this.page.url = '{{ $article->link }}';
          this.page.identifier = '{{ $article->slug }}';
      };

      (function() {
          var d = document, s = d.createElement('script');
          s.src = 'https://cosup.disqus.com/embed.js';
          s.setAttribute('data-timestamp', +new Date());
          (d.head || d.body).appendChild(s);
      })();
  </script>
@endpush