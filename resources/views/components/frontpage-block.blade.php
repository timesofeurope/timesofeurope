@php
  $style = "border-color: #" . $article->section->colour_hex . ";";
  $imageStyle = "";
  $originalUrl = null;
  if (isset($article->cover_image_id)) {
    $url = $article->getMedia("cover-images")[0]->getUrl("frontpage-preview");
    $originalUrl = $article->getMedia("cover-images")[0]->getUrl("frontpage");
    $imageStyle = <<<CSS
    background-image: linear-gradient(
        rgba(0, 0, 0, 0.4),
        rgba(0, 0, 0, 0.4)
      ), url('$url');
CSS
    ;
    $imageStyle = $imageStyle . "background-position: center " . $article->crop_gravity . ";";
  } else {
    $style = $style . "background-color: #" . $article->section->colour_hex . ";";
  }
@endphp
@if (isset($link))
<a href="{{ $article->link }}">
@endif
  <div class="frontpage-block frontpage-block-{{ $width }}" style="{{ $style }}">
    <div class="frontpage-block-image blur" style="{{ $imageStyle }}" data-original-url="{{ $originalUrl }}"></div>
    <div class="frontpage-block-title">{{ $article->title }}</div>
    <div class="frontpage-block-author">
      @if($width === 1)
        {{-- Only enough space for the name, no avatar --}}
        <span class="article-author">{{ $article->user->name }}</span>
        <span class="article-author-school">({{ $article->user->school }})</span>
      @else
        {{-- ALL THE WAY --}}
        <img
            src="{{ $article->user->avatar . '&s=32' }}"
            alt="Avatar of {{ $article->user->name }}"
            class="img-circle"
            width="32"
            height="32"
        >
        <span class="article-author">{{ $article->user->name }}</span>
        <span class="article-author-school">({{ $article->user->school }})</span>
      @endif
    </div>
  </div>
@if (isset($link))
</a>
@endif
