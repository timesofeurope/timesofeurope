<div class="home-article" style="border-color: #{{ $article->section->colour_hex }}">
  <a href="{{ $article->link }}"><h2>{{ $article->title }}</h2></a>
  @if(isset($section) && $article->section->id !== $section->id)
    <div><a href="{{ route('section', $article->section->slug) }}">{{ $article->section->name }}</a></div>
  @endif
  <a href="{{ route('author', $article->user->slug) }}">
    <span class="article-author">{{ $article->user->name }}</span>
    <span class="article-author-school">({{ $article->user->school }})</span>
  </a>
  <div class="excerpt">
    @if (!empty($article->excerpt))
      {!! $article->excerpt !!}
    @else
      {!! \Illuminate\Support\Str::limit(preg_replace('{<\/?(p|br)>}', ' ', $article->content), 300) !!}
    @endif
  </div>
</div>