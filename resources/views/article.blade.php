@extends('layouts.app')

@section('og:type', 'article')
@section('og:title', $article->title . ' - ' . config('app.name'))
@section('og:description', Illuminate\Support\Str::limit(strip_tags($article->content), 400))
@section('og:extra')
  <meta property="article:published_time" content="{{ $article->published_at->toIso8601String() }}">
  <meta property="article:author" content="{{ $article->user->facebook_url ?? route('author', $article->user->slug) }}">
  @if($article->cover_image_id)
    <meta property="og:image" content="{{ $article->getMedia('cover-images')[0]->getUrl('fb-cover') }}">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="630">
  @endif
@endsection

@section('content')
  @include('_public-nav-desktop')
  @include('_public-nav-mobile')
  <div class="article">
    <h1>{{ $article->title }}</h1>
    @if($article->cover_image_id)
      <div class="article-cover-image" style="background-image: url('{{ $article->getMedia('cover-images')[0]->getUrl('fb-cover') }}')"></div>
    @endif
    <div class="article-meta" style="border-bottom-color: #{{ $article->section->colour_hex }}">
      <div>
        <a href="{{ route('author', $article->user->slug) }}">
          <img
              src="{{ $article->user->avatar . '&s=64' }}"
              alt="Avatar of {{ $article->user->name }}"
              class="img-circle"
              width="64"
              height="64"
          >
          <span class="article-author">{{ $article->user->name }}</span>
          <span class="article-author-school">({{ $article->user->school }})</span>
        </a>
      </div>
      <div>
        <a href="{{ route('section', $article->section->slug) }}" class="article-section">
          {{ $article->section->name }}
        </a>
      </div>
      <div>
        <a href="{{ $article->link }}">
          {{ $article->published_at->format('l jS F Y') }}
        </a>
      </div>
      @if (Auth::check() && Auth::user()->can('access manager'))
        <div>
          <a href="{{ route('manager.articles.edit', $article) }}">
            Edit in Manager
          </a>
        </div>
      @endif
    </div>
    <div class="article-content">
      {!! $article->content !!}
    </div>
    @include('components.share-buttons')
    <div class="article-foot-meta">
      <div>
        <img
            src="{{ $article->user->avatar . '&s=64' }}"
            alt="Avatar of {{ $article->user->name }}"
            class="img-circle"
            width="64"
            height="64"
        >
        Written by
        <a href="{{ route('author', $article->user->slug) }}">
          <span class="article-author">{{ $article->user->name }}</span>
          <span class="article-author-school">({{ $article->user->school }})</span>
        </a>
        on
        <a href="{{ $article->link }}">
          {{ $article->published_at->format('l jS F Y') }}
        </a>
      </div>
    </div>
  @include('components.discuss')
  </div>
@endsection