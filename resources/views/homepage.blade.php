@extends('layouts.app')

@section('body-class', 'homepage')

@section('content')
  @include('_public-nav-desktop')
  @include('_public-nav-mobile')
  <div class="home-articles">
    @foreach ($articles as $article)
      @component('home-article', ['article' => $article])
      @endcomponent
    @endforeach
  </div>
@endsection