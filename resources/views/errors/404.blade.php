@extends('layouts.app')

@section('body-class', 'homepage')

@section('content')
  @include('_public-nav-desktop')
  @include('_public-nav-mobile')
  <div class="home-container">
    <h1>Not Found</h1>
    <p>We're sorry, but the page you're looking for cannot be found. Perhaps you mistyped your link?</p>
  </div>
@endsection