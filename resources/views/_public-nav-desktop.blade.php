<div class="home-header hidden-xs hidden-sm">
  <h1>{{ config('app.name') }}</h1>
  <div class="header-sections">
    <div class="header-section" style="--color: #4994D5">
      <a href="/">
        Front Page
      </a>
    </div>
    @foreach ($sections as $section)
      <div class="header-section" style="--color: #{{ $section->colour_hex }}">
        <a href="{{ route('section', $section->slug) }}">
          {{ $section->name }}
        </a>
      </div>
    @endforeach
  </div>
</div>