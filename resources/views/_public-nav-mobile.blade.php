<nav class="navbar navbar-default hidden-md hidden-lg">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse"
              aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/">{{ config('app.name') }}</a>
    </div>

    <div class="collapse navbar-collapse" id="navbar-collapse">
      <ul class="nav navbar-nav">
        <li class="mobile-section" style="border-bottom-color: #4994D5">
          <a href="/">
            Front Page
          </a>
        </li>
        @foreach ($sections as $section)
          <li class="mobile-section" style="border-bottom-color: #{{ $section->colour_hex }}">
            <a href="{{ route('section', $section->slug) }}">
              {{ $section->name }}
            </a>
          </li>
        @endforeach
      </ul>
    </div>
  </div>
</nav>