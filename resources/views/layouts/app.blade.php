<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <meta property="og:site_name" content="{{ config('app.name') }}" />
  <meta property="og:type" content="@yield('og:type', 'website')" />
  <meta property="og:title" content="@yield('og:title', config('app.name'))" />
  <meta property="og:description" content="@yield('og:description', View::make('layouts.og-description'))" />
  <meta property="og:url" content="{{ Request::url() }}">
  <meta property="fb:app_id" content="{{ env('FACEBOOK_ID') }}">
  @yield('og:extra')

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Laravel') }}</title>

  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  @stack('header-styles')
</head>
<body class="@yield('body-class')">

@if (Auth::check() && !str_contains(Route::currentRouteName(), 'manager'))
  <a href="{{ route('manager.home') }}">Manager</a>
@endif

<div id="app">
  @yield('content')
</div>

<!-- Scripts -->
@stack('body-scripts-before-app')
<script src="{{ asset('js/app.js') }}"></script>
@stack('body-scripts')
</body>
</html>
