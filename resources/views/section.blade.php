@extends('layouts.app')

@section('body-class', 'homepage')

@section('content')
  @include('_public-nav-desktop')
  @include('_public-nav-mobile')
  <div class="home-articles">
    <h1>{{ $section->name }}</h1>
    @if(isset($section->description))
      <div>
        {{ $section->description }}
      </div>
    @endif
    @foreach ($articles as $article)
      @component('home-article', ['article' => $article, 'section' => $section])
      @endcomponent
    @endforeach
  </div>
@endsection