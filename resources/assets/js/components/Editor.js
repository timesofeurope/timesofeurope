import React from "react";
import ReactDOM from "react-dom";
import Flatpickr from "react-flatpickr";
import ReactQuill, { Quill } from "react-quill";
import idx from "idx";
import slugify from "slugify";
import axios from "axios";
import { Line } from "react-progressbar.js";
import { Modal } from "react-bootstrap";
import { toPairs } from "lodash";

class Editor extends React.Component {
  constructor(props) {
    super(props);

    this.quill = null;
    this.unloadHandler = null;
    this.coverImageField = null;

    const denormalizeIds = array => {
      return array.reduce(
        (prev, curr) => Object.assign(prev, { [curr.id]: curr }),
        {}
      );
    };

    const pubDate = idx(props, _ => _.article.published_at);

    this.state = {
      error: null,
      title: idx(props, _ => _.article.title) || "",
      sectionId: idx(props, _ => _.article.section_id) || 1,
      userId: idx(props, _ => _.article.user_id) || props.user.id,
      content: idx(props, _ => _.article.content) || "",
      previewToken: idx(props, _ => _.article.preview_token) || "",
      publishedAt: pubDate ? new Date(pubDate) : new Date(),
      sections: denormalizeIds(props.sections),
      protected: !props.protected
        ? null
        : {
            users: denormalizeIds(props.protected.users)
          },
      uploadProgress: null,
      dirty: false,
      previewHolderId: idx(props, _ => _.previewHolder.id) || null,
      previewHolderSlug: idx(props, _ => _.previewHolder.slug) || null,
      previewHolderPreviewToken:
        idx(props, _ => _.previewHolder.preview_token) || null,
      submittingOpen: false,
      changeCoverImage: !idx(props, _ => _.article.cover_image_id),
      cropGravity: idx(props, _ => _.article.crop_gravity) || "center",
      showHistory: false,
      history: null
    };
  }

  componentDidMount() {
    if (this.unloadHandler === null) {
      this.unloadHandler = (e => {
        if (this.state.dirty) {
          const message =
            "You have unsaved changes. Are you sure you want to close this page?";
          e.returnValue = message;
          return message;
        }
      }).bind(this);
    }
    window.addEventListener("beforeunload", this.unloadHandler);
  }

  componentWillUnmount() {
    if (this.unloadHandler !== null) {
      window.removeEventListener("beforeunload", this.unloadHandler);
      this.unloadHandler = null;
    }
  }

  submit = (payload, callback = null) => {
    axios
      .post(
        route(
          "manager.articles.edit.save",
          payload.get("article_id") || "null"
        ),
        payload,
        {
          headers: {
            "Content-Type": "application/json"
          }
        }
      )
      .then(response => {
        if (response.data.error) {
          this.setState({
            error: `Error saving: ${response.data.message}`,
            submittingOpen: false
          });
        } else {
          if (callback) {
            callback(response);
          } else {
            this.setState({
              dirty: false,
              submittingOpen: false
            });
            window.location = route(
              "manager.articles.edit",
              response.data.article.id
            );
          }
        }
      })
      .catch(ex => {
        this.setState({
          error: `Error saving: ${ex}`,
          submittingOpen: false
        });
      });
  };

  makePayload = (extras = {}) => {
    const data = Object.assign(
      {},
      {
        article_id: this.props.article.id || "null",
        title: this.state.title,
        section_id: this.state.sectionId,
        user_id: this.state.userId,
        crop_gravity: this.state.cropGravity,
        content: this.quill.getEditor().container.querySelector(".ql-editor")
          .innerHTML, // ow
        published_at: this.state.publishedAt.valueOf() / 1000,
        preview_holder_id: this.state.previewHolderId || undefined,
      },
      extras
    );
    const object = new FormData();
    for (let key in data) {
      if (data.hasOwnProperty(key)) {
        object.append(key, data[key] || null);
      }
    }
    if (this.state.changeCoverImage && this.coverImageField.files.length > 0) {
      object.append("cover_image", this.coverImageField.files[0]);
    }
    return object;
  };

  closerButton = action => {
    return () => {
      this.setState({
        submittingOpen: true
      });
      this.submit(this.makePayload({ action }));
    };
  };

  discard = () => {
    window.location = route("manager.articles.index");
  };

  goToArticle = () => {
    window.open(
      route("articles.show", {
        slug: this.props.article.slug,
        name: slugify(this.props.article.title, {
          lower: true,
          remove: /[$*_+~.()'"!\-:@]/g
        })
      }),
      "_blank"
    );
  };

  openPreview = () => {
    if (!this.state.dirty) {
      // We can go straight to the preview
      window.open(
        route("manager.articles.preview", {
          slug: this.props.article.slug,
          token: this.props.article.preview_token
        }),
        "_blank"
      );
    } else {
      // Grr...
      // Create a preview holder, or update it if it exists, and open that
      let previewWindow = window.open(
        route("manager.articles.preview.loading"),
        "_blank"
      );
      this.submit(
        this.makePayload({
          title: this.state.title + " (preview)",
          article_id: this.state.previewHolderId || "null",
          preview_parent_id: this.props.article.id || "null",
          action: "createPreviewHolder"
        }),
        response => {
          this.setState({
            previewHolderId: response.data.article.id,
            previewHolderSlug: response.data.article.slug,
            previewHolderPreviewToken: response.data.article.preview_token
          });
          previewWindow.location.href = route("manager.articles.preview", {
            slug: response.data.article.slug,
            token: response.data.article.preview_token
          });
        }
      );
    }
  };

  setSection = event => {
    this.setState({
      sectionId: event.target.value,
      dirty: true
    });
  };

  changeTitle = event => {
    this.setState({
      title: event.target.value,
      dirty: true
    });
  };

  setUser = event => {
    this.setState({
      userId: event.target.value,
      dirty: true
    });
  };

  setPublishedAt = value => {
    this.setState({
      publishedAt: value[0],
      dirty: true
    });
  };

  changeCoverImage = () => {
    this.setState({
      changeCoverImage: true
    });
  };

  onChange = value => {
    this.setState({
      dirty: true
    });
  };

  changeCropGravity = event => {
    this.setState({
      cropGravity: event.target.value
    });
  };

  showHistory = async () => {
    this.setState({
      showHistory: true
    });
    if (this.state.history === null) {
      const response = await axios.get(route("api.manager.article.history", {id: this.props.article.id}));
      this.setState({
        history: response.data.history
      });
    }
  };

  hideHistory = () => {
    this.setState({
      showHistory: false
    });
  };

  getQuillModules() {
    return {
      toolbar: {
        container: [
          [{ header: [1, 2, false] }],
          ["bold", "italic", "underline", "strike", "blockquote"],
          [{ list: "ordered" }, { list: "bullet" }],
          ["link", "image"],
          ["clean"]
        ]
      }
    };
  }

  getQuillFormats() {
    return [
      "header",
      "bold",
      "italic",
      "underline",
      "strike",
      "blockquote",
      "list",
      "bullet",
      "link",
      "image"
    ];
  }

  renderButtons() {
    return (
      <div className="button-group btns">
        <button className="btn btn-primary" onClick={this.closerButton("save")}>
          Save
        </button>
        <button className="btn btn-default" onClick={this.discard}>
          Discard
        </button>
        {this.props.article.state === "published" &&
          <button className="btn btn-default" onClick={this.goToArticle}>
            View
          </button>}
        <button className="btn btn-default" onClick={this.openPreview}>
          Preview
        </button>
        {this.props.userPermissions["change front page"] &&
          (this.props.article.state === "published"
            ? <button
                className="btn btn-danger"
                onClick={this.closerButton("yank")}
              >
                Yank
              </button>
            : <button
                className="btn btn-success"
                onClick={this.closerButton("publish")}
              >
                {this.state.userId === this.props.user.id
                  ? "Direct Inject"
                  : "Publish"}
              </button>)}
        {this.props.article.id && (
            <button className="btn btn-default" onClick={this.showHistory}>History</button>
        )}
      </div>
    );
  }

  renderTitle() {
    return (
      <input
        type="text"
        className="article-title-field"
        placeholder="Title"
        value={this.state.title}
        onChange={this.changeTitle}
        required
      />
    );
  }

  renderMeta() {
    return (
      <div className="article-meta">
        <label>
          Section
          <select
            className="form-control"
            value={this.state.sectionId}
            onChange={this.setSection}
          >
            {this.props.sections.map(section =>
              <option key={section.id} value={section.id}>
                {section.name}
              </option>
            )}
          </select>
        </label>
        <label>
          Published At
          <Flatpickr
              className="form-control"
            options={{ enableTime: true }}
            value={this.state.publishedAt}
            onChange={this.setPublishedAt}
          />
        </label>
        {this.state.changeCoverImage
          ? <label>
              Cover Image
              <input
                  className="form-control"
                type="file"
                name="cover_image"
                ref={c => {
                  this.coverImageField = c;
                }}
              />
            </label>
          : <button onClick={this.changeCoverImage} className="btn btn-default">
              CHANGE COVER IMAGE
            </button>}
        <label>
          Cover Image Crop Gravity
          <select
              className="form-control"
            value={this.state.cropGravity}
            onChange={this.changeCropGravity}
          >
            <option value="top">Top</option>
            <option value="center">Center</option>
            <option value="bottom">Bottom</option>
          </select>
        </label>
        {this.props.userPermissions["edit protected properties"] &&
          <label>
            Author
            <select
              className="form-control"
              value={this.state.userId}
              onChange={this.setUser}
            >
              {this.props.protected.users.map(user =>
                <option key={user.id} value={user.id}>
                  {user.name}
                </option>
              )}
            </select>
          </label>
          }
      </div>
    );
  }

  renderEditor() {
    return (
      <div key="do-not-remount">
        {this.state.uploadProgress &&
          <Line
            key="progress"
            progress={this.state.uploadProgress / 100}
            options={{ color: "#26BFD8" }}
          />}
        <ReactQuill
          key="quill"
          defaultValue={this.state.content}
          modules={this.getQuillModules()}
          formats={this.getQuillFormats()}
          onChange={this.onChange}
          ref={c => {
            this.quill = c;
          }}
        />
      </div>
    );
  }

  renderModals() {
    return [
      <div key="submitting">
        <Modal show={this.state.submittingOpen}>
          <Modal.Header>
            <Modal.Title>Just a second...</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <p>Saving your article, please wait</p>
          </Modal.Body>
        </Modal>
      </div>,
      <div key="history">
        <Modal show={this.state.showHistory} onHide={this.hideHistory}>
          {(this.state.history === null) ? (
              <div>
                <Modal.Header closeButton><Modal.Title>Just a second</Modal.Title></Modal.Header>
                <Modal.Body><p>Loading article history, please wait...</p></Modal.Body>
              </div>
          ) : (
              <div>
                <Modal.Header closeButton>
                  <Modal.Title>Article History</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                  {this.state.history.map(revision => (
                      <div className="revision" key={revision.revision.id}>
                        <div className="revision-author">
                          By <strong>{revision.user.name}</strong>
                        </div>
                        {toPairs(revision.changes).map(kvPair => {
                          const [field, change] = kvPair;
                          return (
                              <div key={field}>
                                <span className="change-field">{field}</span>
                                <table className="table">
                                  <tbody>
                                    <tr>
                                      <td style={{ background: "#ffe9e9"}} className="deleted-line">{ change.old }</td>
                                      <td style={{ background: "#e9ffe9"}} className="added-line">{ change.new }</td>
                                    </tr>
                                  </tbody>
                                </table>
                              </div>
                        );
                        })}
                      </div>
                  ))}
                </Modal.Body>
              </div>
          )}
        </Modal>
      </div>
    ];
  }

  render() {
    return (
      <div className="article-editor">
        {this.state.error &&
          <div className="alert alert-danger">
            {this.state.error}
          </div>}
        {this.renderButtons()}
        {this.renderTitle()}
        {this.renderMeta()}
        {this.renderEditor()}
        {this.renderModals()}
      </div>
    );
  }
}

export default function(container, data) {
  ReactDOM.render(<Editor {...data} />, container);
}
