import React from "react";
import ReactDOM from "react-dom";
import SortableTree, {
  getTreeFromFlatData,
  getFlatDataFromTree
} from "react-sortable-tree";
import { Glyphicon, Button } from "react-bootstrap";
import SectionEditor from "./SectionEditor";

class SortableSections extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      flatData: props.sections,
      items: getTreeFromFlatData({
        flatData: props.sections.map(node => ({ ...node, title: node.name })),
        getParentKey: node => node.parent_id,
        rootKey: null
      }),
      editing: null
    };
  }

  makeEdit = id => () => {
    this.setState({
      editing: id
    });
  };

  cancelEdit = () => {
    this.setState({
      editing: null
    });
  };

  onChange = treeData => {
    this.setState({
      items: treeData
    });
  };

  submit = () => {
    const data = getFlatDataFromTree({
      treeData: this.state.items,
      getNodeKey: ({ node }) => node.id,
      ignoreCollapsed: false
    }).map(({ node, path }) => ({
      id: node.id,

      // The last entry in the path is this node's key
      // The second to last entry (accessed here) is the parent node's key
      parent: path.length > 1 ? path[path.length - 2] : null
    }));

    const form = document.createElement("form");
    form.method = "POST";
    form.action = route("manager.sections.reorder");

    data.forEach((item, index) => {
      const nodeParent = document.createElement("input");
      nodeParent.type = "hidden";
      nodeParent.name = `sections[${item.id}][parent_id]`;
      nodeParent.value = item.parent || "null";
      form.appendChild(nodeParent);
      const nodeOrder = document.createElement("input");
      nodeOrder.type = "hidden";
      nodeOrder.name = `sections[${item.id}][order]`;
      nodeOrder.value = index;
      form.appendChild(nodeOrder);
    });

    const nodeCsrf = document.createElement("input");
    nodeCsrf.type = "hidden";
    nodeCsrf.name = "_token";
    nodeCsrf.value = document.head.querySelector(
      'meta[name="csrf-token"]'
    ).content;
    form.appendChild(nodeCsrf);

    document.body.appendChild(form);
    form.submit();
  };

  createNew = (parentId, order) => () => {
    console.log(parentId);
    this.setState(state => ({
      flatData: [
        ...state.flatData,
        {
          id: state.flatData[state.flatData.length - 1].id + 1,
          name: "",
          colour_hex: "",
          parent_id: parentId,
          order,
          new: true
        }
      ],
      editing: state.flatData[state.flatData.length - 1].id + 1
    }));
  };

  render() {
    return (
      <div>
        {this.state.flatData.map((item, index) => [
          <SectionEditor
            key={`${item.id}editor`}
            show={item.id === this.state.editing}
            section={item}
            cancel={this.cancelEdit}
          />
        ])}
        <div>
          <Button onClick={this.submit}>Save</Button>
        </div>
        <div style={{ height: 500 }}>
          <SortableTree
            treeData={this.state.items}
            className="sortable-tree"
            onChange={this.onChange}
            getNodeKey={({ node }) => node.id}
            maxDepth={2}
            generateNodeProps={({ node, path }) => ({
              buttons: [
                <a href="#" onClick={this.makeEdit(node.id)}>
                  <Glyphicon glyph="pencil" />
                </a>,
                path.length === 1 &&
                  <a href="#" onClick={this.createNew(path[0], node.order + 1)}>
                    &nbsp;
                    <Glyphicon glyph="plus" />
                  </a>
              ]
            })}
          />
        </div>
      </div>
    );
  }
}

export default (container, sections) => {
  ReactDOM.render(<SortableSections sections={sections} />, container);
};
