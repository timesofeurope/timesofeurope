import React from "react";
import ReactDOM from "react-dom";

class PasswordField extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            score: null
        };
    }

    onType = async event => {
        const value = event.target.value;
        require.ensure(['zxcvbn'], require => {
            const zxcvbn = require('zxcvbn');
            const result = zxcvbn(value);
            this.setState({
                score: result.score,
                guessTime: result.crack_times_display.offline_slow_hashing_1e4_per_second,
                warning: result.feedback.warning,
                suggestions: result.feedback.suggestions
            });
        });
    };

    render() {
        return (
            <div>
                <input type="password" className="form-control" name={this.props.name} onChange={this.onType}/>
                {this.state.score !== null && (
                    <div>
                        <div>Your password would be guessed in approximately <em>{this.state.guessTime}</em></div>
                        {this.state.warning && (
                            <div className="text-warning">{this.state.warning}</div>
                        )}
                        {this.state.suggestions.length > 0 && (
                            <div>
                                Here are some suggestions to improve your password:
                                <ul>
                                    {this.state.suggestions.map(x => <li key={x}>{x}</li>)}
                                </ul>
                            </div>
                        )}
                    </div>
                )}
            </div>
        );
    }
}

function mountPasswordField(node, name) {
    ReactDOM.render(<PasswordField name={name}/>, node);
}

export default mountPasswordField;
