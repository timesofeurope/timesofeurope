import React from "react";
import ReactDOM from "react-dom";
import axios from "axios";
import {Pagination, PaginationButton, Well, Alert} from "react-bootstrap";
import {makePreview} from "../image-preview";
import * as jQuery from "jquery";
import {keyBy} from "lodash";

class Frontpage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      frontpages: [
        {
          data: props.frontpage.data,
          score: 0
        }
      ],
      currentFrontpage: 0,
      loadingVariant: false,
      error: null,
      blocks: {},
      meta: {}
    };
  }

  componentDidMount() {
    this.loadBlocks(
        this.state.frontpages[this.state.currentFrontpage].data.flatten()
    );
  }

  async loadBlocks(data) {
    data.forEach(async pair => {
      if (`${pair[0]}-${pair[1]}` in this.state.blocks) {
        return;
      }
      const [articleId, width] = pair;
      const response = await axios.get(
          route("api.manager.frontpage.getBlockFor", {id: articleId}),
          {
            params: {width, link: false}
          }
      );
      this.setState(state => ({
        blocks: {
          ...state.blocks,
          [`${articleId}-${width}`]: response.data
        }
      }));
    });
  }

  generateSeed = () => {
    const seed = prompt("Please specify a seed");
    if (seed === null) {
      return;
    }
    return this.generateVariant(seed)();
  };

  generateVariant = (seed = undefined) => async () => {
    this.setState({
      loadingVariant: true,
      error: null
    });
    try {
      const response = await axios.get(
          route("api.manager.frontpage.generateVariant"),
          {
            params: {seed}
          }
      );
      this.setState(state => ({
        loadingVariant: false,
        frontpages: [...state.frontpages, response.data.data],
        currentFrontpage: state.currentFrontpage + 1,
        meta: response.data.meta
      }));
      this.loadBlocks(response.data.data.data.flatten());
    } catch (ex) {
      this.setState({
        error: ex
      });
    }
  };

  changeFrontpage = index => _ => {
    this.setState({
      currentFrontpage: index
    });
    this.loadBlocks(this.state.frontpages[index].data.flatten());
  };

  makePreview = e => {
    makePreview(e);
  };

  save = async () => {
    try {
      const response = await axios.post(route("api.manager.frontpage.save"), {
        data: this.state.frontpages[this.state.currentFrontpage].data
      });

      if (response.data.error) {
        this.setState({
          error: response.data.error
        });
      } else {
        window.location.reload();
      }
    } catch (ex) {
      this.setState({error: ex});
    }
  };

  renderBlock = (pair, index) => {
    const [articleId, width] = pair;
    let contents;

    if (this.state.blocks[`${articleId}-${width}`]) {
      contents = (
          <div
              dangerouslySetInnerHTML={{
                __html: this.state.blocks[`${articleId}-${width}`]
              }}
              ref={r => this.makePreview(r)}
          />
      );
    } else {
      contents = (
          <div className={`frontpage-block frontpage-block-${width}`}>
            LOADING
          </div>
      );
    }
    return contents;
  };

  renderFrontpage = data =>
      <Well style={{maxWidth: 800}}>
        <div className="frontpage">
          {data.map((row, index) =>
              <div className="frontpage-row" key={`row${index}`}>
                {row.map((pair, index) => this.renderBlock(pair, index))}
              </div>
          )}
        </div>
      </Well>;

  render() {
    if (this.state.error) {
      return (
          <Alert bsStyle="error">
            <h4>An error occurred!</h4>
            <p>Please try again or contact the system administrator</p>
            <p>
              Error information:
              <code>{JSON.stringify(this.state.error.response ? this.state.error.response.data.exception : this.state.error)}</code>
            </p>
          </Alert>
      );
    }

    return (
        <div style={{maxWidth: 800}}>
          <h2>Variants</h2>
          <Pagination>
            {this.state.frontpages.map((_, index) =>
                <Pagination.Item
                    active={this.state.currentFrontpage === index}
                    onClick={this.changeFrontpage(index)}
                >
                  {index + 1}
                </Pagination.Item>
            )}
            {this.state.loadingVariant
                ? <Pagination.Next disabled>Loading...</Pagination.Next>
                : [
                  <Pagination.Next onClick={this.generateVariant()}>
                    Generate New
                  </Pagination.Next>,
                  <Pagination.Next onClick={this.generateSeed}>
                    Generate With Given Seed
                  </Pagination.Next>
                ]}
            <Pagination.Next onClick={this.save}>Set this variant as front page</Pagination.Next>
          </Pagination>
          {this.state.currentFrontpage !== 0 &&
          <div>
            <strong>
              Score: {this.state.frontpages[this.state.currentFrontpage].score}
            </strong>
          </div>}
          {Object.keys(this.state.meta).length > 0 &&
          <div>
            Meta:
            <code>{JSON.stringify(this.state.meta)}</code>
          </div>}
          {this.renderFrontpage(
              this.state.frontpages[this.state.currentFrontpage].data
          )}
        </div>
    );
  }
}

export default function (container, data) {
  ReactDOM.render(<Frontpage {...data} />, container);
}
