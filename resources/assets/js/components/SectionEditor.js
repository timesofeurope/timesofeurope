import React from "react";
import { Modal, Alert } from "react-bootstrap";
import { SketchPicker } from "react-color";

const PRESETS = [
  "#3AB21D",
  "#D83232",
  "#F37942",
  "#7827F2",
  "#cbb956",
  "#F236C0",
  "#DE5168"
];

export default class SectionEditor extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: props.section.name,
      color: "#" + props.section.colour_hex,
      parent_id: props.section.parent_id || null,
      new: props.section.new || false,
      description: props.section.description || "",
      error: null
    };
  }

  updateName = e => {
    this.setState({
      name: e.target.value
    });
  };

  updateDescription = e => {
    this.setState({
      description: e.target.value
    });
  };

  changeColor = newColor => {
    this.setState({
      color: newColor.hex
    });
  };

  save = () => {
    return axios
      .post(
        route(
          this.state.new
            ? "api.manager.section.new"
            : "api.manager.section.edit",
          this.props.section.id
        ),
        {
          ...this.props.section,
          name: this.state.name,
          colour_hex: this.state.color.replace("#", ""),
          description: this.state.description
        }
      )
      .then(response => {
        console.log(response.data);
        if (response.data.error) {
          this.setState({
            error: response.data.message
          });
        } else {
          window.location.reload();
        }
      })
      .catch(error => {
        this.setState({ error });
      });
  };

  render() {
    return (
      <Modal show={this.props.show}>
        <Modal.Header>
          <Modal.Title>
            {this.state.new ? "Creating" : "Editing"} {this.state.name}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {this.state.error &&
            <Alert bsStyle="danger">
              {this.state.error}
            </Alert>}
          <div>
            <label>
              Name
              <input
                type="text"
                className="form-control"
                value={this.state.name}
                onChange={this.updateName}
              />
            </label>
          </div>
          <label>
            <div>Description</div>
            <div>
              <textarea
                rows={4}
                cols={60}
                value={this.state.description}
                onChange={this.updateDescription}
              />
            </div>
          </label>
          <SketchPicker
            color={this.state.color}
            onChangeComplete={this.changeColor}
            presetColors={PRESETS}
          />
          <button className="btn btn-primary" onClick={this.save}>
            SAVE
          </button>
          <button className="btn btn-default" onClick={this.props.cancel}>
            CANCEL
          </button>
        </Modal.Body>
      </Modal>
    );
  }
}
