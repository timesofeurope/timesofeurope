function doIt(element) {
  const newUrl = element.attributes["data-original-url"].value;
  if (!newUrl) {
    return;
  }
  const img = new Image();
  const originalUrl = element.style.backgroundImage.match(/url\(['"](.+)['"]\)/)[1];
  img.onload = () => {
    element.style.backgroundImage = element.style.backgroundImage.replace(originalUrl, newUrl);
    $(element).removeClass("blur");
  };
  img.src = newUrl;
}

export function makePreview(element) {
  const collection = $(element).find('[data-original-url]');
  if (collection.length > 0) {
    return doIt(collection.get(0));
  }
}

$(() => {
  $('[data-original-url]').each((index, element) => {
    doIt(element);
  });
});
