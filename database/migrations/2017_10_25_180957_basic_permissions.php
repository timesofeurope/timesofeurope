<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class BasicPermissions extends Migration
{
    private $permNames = [
        'access manager',
        'write articles',
        'edit any article',
        'change front page',
        'edit any user profile',
        'change any user roles',
        'invite new users'
    ];

    private $roleNames = [
        'writer',
        'editor',
        'chief editor',
        'system administrator'
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Permission::insert(array_map(function ($n) {
            return ['guard_name' => 'web', 'name' => $n];
        }, $this->permNames));

        Role::insert(array_map(function ($n) {
            return ['guard_name' => 'web', 'name' => $n];
        }, $this->roleNames));

        Role::findByName('writer')->givePermissionTo('access manager', 'write articles');
        Role::findByName('editor')->givePermissionTo('access manager', 'write articles', 'edit any article');
        Role::findByName('chief editor')->givePermissionTo('access manager', 'write articles', 'edit any article', 'change front page', 'edit any user profile', 'invite new users');
        Role::findByName('system administrator')->givePermissionTo($this->permNames);

        $admin = \App\User::where('name', 'System Administrator')->first();
        if (!empty($admin)) {
            $admin->assignRole('system administrator');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Role::whereIn('name', $this->roleNames)->delete();
        Permission::whereIn('name', $this->permNames)->delete();
    }
}
