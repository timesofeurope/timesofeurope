<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermEditProtectedProperties extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Permission::create([
            'name' => 'edit protected properties',
            'guard_name' => 'web'
        ]);

        Role::findByName('chief editor')->givePermissionTo('edit protected properties');
        Role::findByName('system administrator')->givePermissionTo('edit protected properties');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Permission::where('name', 'edit protected properties')->delete();
    }
}
