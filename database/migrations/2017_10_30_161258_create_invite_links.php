<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInviteLinks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invite_links', function (Blueprint $table) {
           $table->increments('id');
           $table->unsignedInteger('user_id');
           $table->string('token');
           $table->integer('uses')->default(0);
           $table->integer('max_uses')->default(0);
           $table->timestamp('expires_at')->nullable();
           $table->unsignedInteger('role_id');
           $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('invite_links');
    }
}
