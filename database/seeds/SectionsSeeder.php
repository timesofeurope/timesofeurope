<?php

use App\Section;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SectionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['name' => 'News From Brussels', 'colour_hex' => '3AB21D'],
            ['name' => 'European Schools', 'colour_hex' => 'D83232'],
            ['name' => 'Politics', 'colour_hex' => 'F37942'],
            ['name' => 'Media', 'colour_hex' => '7827F2'],
            ['name' => 'Fluff', 'colour_hex' => 'F236C0'],
        ];

        foreach ($data as $row) {
            $a = new Section($row);
            $a->save();
        }
    }
}
