<?php

use App\User;
use Illuminate\Database\Seeder;

class SystemAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (config('app.env') !== 'production') {
            User::create([
                'name' => 'System Administrator',
                'email' => 'admin@cosup.bullshit',
                'password' => bcrypt('admin'),
                'school' => 'Luxembourg 1'
            ]);
            User::where('name', 'System Administrator')->first()->assignRole('system administrator');
        }
    }
}
